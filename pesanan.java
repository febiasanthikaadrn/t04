class pesanan extends kue{

    private double jumlah;
    private double berat;

    public pesanan(String name, double price, int berat){
        super(name, price);
        this.berat = berat;
    }

    public int getBerat(){
        return 0;
    }

    public void setBerat(int berat){
        this.berat = berat;
    }

    @Override
    public double hitungHarga(){
        return super.getPrice() * berat;
    }

    @Override
    public double Berat(){
        return berat;
    }

    @Override
    public double Jumlah(){
        return jumlah;
    }

    @Override
    public String toString(){
        return super.toString() + String.format("\nBerat\t\t:%.2f\nTotal harga\t:%.2f", berat, hitungHarga());
    }
}
