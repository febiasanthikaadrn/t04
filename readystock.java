class readystock extends kue{

    private double jumlah;
    private double berat;

    public readystock(String name, double price, int jumlah) {
        super(name, price);
        this.jumlah = jumlah;
    }

    public int getJumlah() {
        return 0;
    }

    public void setJumlah(int jumlah) {
        this.jumlah = jumlah;
    }

    @Override
    public double hitungHarga() {
        return super.getPrice() * jumlah * 2;
    }

    @Override
    public double Berat() {
        return berat;
    }

    @Override
    public double Jumlah() {
        return jumlah;
    }

    @Override
    public String toString() {
        return super.toString() + String.format("\nJumlah\t\t:%.2f\nTotal harga\t:%.2f", jumlah, hitungHarga());
    }
}